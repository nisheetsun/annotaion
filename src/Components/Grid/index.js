import React from "react";
import "./index.css";
import Point from '../Points'

export default class Grid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: null
    };
  }

  onImgLoad = ({ target: img }) => {
    this.setStateWrapper({
      imageDimentions: { height: img.offsetHeight, width: img.offsetWidth },
      imageLoaded: true,
      hoveredPoints:[]
    });
  };

  setStateWrapper = (key_value_dict) => {
    this.setState(key_value_dict)
  }

  onHover = (e) => {
    // e.screenX
    let x = this.state.hoveredPoints
    x.push([e.screenX, e.screenY])
    this.setStateWrapper({hoveredPoints:x})
  }

  testcolumn = (row_num, _num_col) => {
    let _num = _num_col
    let _list = [];
    let i = 0;
    // if (row_num%2!=0){
    //   _num = _num - 1
    // }
    for (i = 0; i < _num-1; i++) {
        // _list.push(<span onMouseOver={this.onHover}></span>);
        _list.push(<Point removeHoveredPoints={this.props.removeHoveredPoints} row_column={row_num.toString()+'_'+i.toString()} addToHoveredPoints={this.props.addToHoveredPoints} is_mousedown={this.props.is_mousedown}/>);
    }
    return _list
  };

  testrow = () => {
    let _num = Math.floor(this.props.imageDimentions.width / 12);
    let _num_col = Math.floor(this.props.imageDimentions.width / 12);
    _num = 20
    let _list = [];
    let i = 0;
    for (i = 0; i < _num; i++) {
      _list.push(<div style={{
        // borderWidth:0.2, borderStyle:'solid', borderColor:'red'
      }} onMouseDown={()=>{}}>{this.testcolumn(i, _num_col)}</div>);
    }
    // console.log("!!!", _list)
    if(this.props.rows_columns_data.length == 0){

    }
    return _list
  };

  componentDidMount() {}
  componentWillUnmount() {}
  render() {
    return (
    <React.Fragment >{this.testrow()}</React.Fragment>
    // <Point></Point>
    );
  }
}
