import React from "react";
import "./index.css";

export default class Point extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: 'grey',
    };
    [this.row, this.column] = this.props.row_column.split('_');
  }

  setStateWrapper = (key_value_dict) => {
    this.setState(key_value_dict)
  }

  onHover = (e) => {
      if(this.props.is_mousedown && this.state.backgroundColor!='red'){
        this.setStateWrapper({backgroundColor:'red'})
        
        this.props.addToHoveredPoints(e.screenX, e.screenY, parseInt(this.row), parseInt(this.column))
      }
  }

  onClick = (e) => {
      if(this.state.backgroundColor == 'red'){
          this.props.removeHoveredPoints(e.screenX, e.screenY, parseInt(this.row), parseInt(this.column))
          this.setState({backgroundColor:'grey'})
      }
  }

  componentDidMount() {
      if(this.props.backgroundColor){
          this.setStateWrapper({backgroundColor: this.props.backgroundColor})
      }
  }
  componentWillUnmount() {}
  render() {
    return (
        <span style={{backgroundColor: this.state.backgroundColor}} onClick={this.onClick} onMouseOver={this.onHover}></span>
    );
  }
}
