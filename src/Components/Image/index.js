import React from "react";
import "./index.css";
import Grid from '../Grid'

var colors = ['green', 'black', 'pink']
var hovered_points = []
var hovered_coordinated = []

export default class Image extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageLoaded: false,
      imageDimentions: { width: null, height: null },
      boxDimention: { width: null, height: null },
      
      history_row_columns: [],
      history_coordinated: [],
      coordinates_label_mapping:[],
      hoveredPoints: [],
      is_mousedown: false,

      rows_columns_data: [],

      table_to_coordinates_mapping: {},
      labels: ['label1', 'label2', 'label3']

    };
  }

  onImgLoad = ({ target: img }) => {
    this.setStateWrapper({
      imageDimentions: { height: img.offsetHeight, width: img.offsetWidth },
      imageLoaded: true,
      hoveredPoints:[]
    });
  };

  setStateWrapper = (key_value_dict) => {
    this.setState(key_value_dict)
  }

  // tablecolumn = () => {
  //   let _num = Math.floor(this.state.imageDimentions.width / 20);
  //   console.log(_num, this.state.imageDimentions.width);
  //   _num = 40;
  //   let _list = [];
  //   let i = 0;
  //   for (i = 0; i < _num; i++) {
  //     _list.push(
  //       <div class="column" style={{ backgroundColor: "transparent" }}>
  //         <div
  //           style={{
  //             // width: 0,
  //             // height: 0,
  //             borderStyle: "solid",
  //             borderWidth: "1px",
  //             borderColor: "red",
  //           }}
  //           onClick={() => {
  //             alert("sss");
  //           }}
  //         ></div>
  //       </div>
  //     );
  //   }
  //   return _list;
  // };

  // tablerow = () => {
  //   let _num = Math.floor(this.state.imageDimentions.height / 20);
  //   // alert(this.state.imageDimentions.height)
  //   let _list = [];
  //   let i = 0;
  //   for (i = 0; i < _num - 1; i++) {
  //     _list.push(<div class="row">{this.tablecolumn()}</div>);
  //   }
  //   return _list;
  // };


  testcolumn = (row_num, which) => {
    let _num = Math.floor(this.state.imageDimentions.width / 12);
    let _list = [];
    let i = 0;
    // if (row_num%2!=0){
    //   _num = _num - 1
    // }
    for (i = 0; i < _num-1; i++) {
      // if ( row_num%2!=0 && i==0){
      //   _list.push(<span style={{marginLeft:6}}></span>)
      // }else{
        if (which == 2){_list.push(<span style={{ position: 'relative', zIndex: 10, backgroundColor:'green'}} onClick={()=>{}} onMouseOver={this.onHover}></span>);}
        else{_list.push(<span style={{position: 'relative', zIndex: 10, backgroundColor:'red'}} onClick={()=>{}} onMouseOver={this.onHover}></span>);}
      // }
    }
    return _list
  };

  testrow = (which) => {
    let _num = Math.floor(this.state.imageDimentions.width / 12);
    _num = 20
    let _list = [];
    let i = 0;
    for (i = 0; i < _num; i++) {
      _list.push(<div style={{
        borderWidth:0.2, borderStyle:'solid', borderColor:'red'
      }} >{this.testcolumn(i, which)}</div>);
    }
    console.log("!!!", _list)
    return _list
  };

  handleChange = (event)=> {
    this.setState({value: event.target.value});
  }
  handleSubmit = (event)=> {
    console.log("!!!!", hovered_points, "!!!!", hovered_coordinated)
    event.preventDefault();
  }

  removeHoveredPoints = (x, y, row, column) => {
    let pindex = hovered_points.findIndex(i => { return i[0] === row && i[1] === column});
    console.log('removeHoveredPoints',hovered_points, row, column, pindex)
    delete hovered_points[pindex]
    let cindex = hovered_coordinated.findIndex(i => { return i[0] === x && i[1] === y});
    delete hovered_coordinated[cindex]
  }

  addToHoveredPoints = (x, y, row, column) => {
    hovered_points.push([row, column])
    hovered_coordinated.push([x, y])

    // let _map = this.state.table_to_coordinates_mapping
    // if(row in _map){
    //   if(column in _map[row]){
    //     // _map[row][column] = [x, y]
    //   }else{
    //     _map[row][column] = [x, y]
    //   }
    // }else{
    //   _map[row] = {}
    //   _map[row][column] = [x, y]
    // }
    // // console.log("#########", x, y, row, column)
    // let _list = this.state.hoveredPoints
    // _list.push([x, y])
    // this.setStateWrapper({hoveredPoints:_list, table_to_coordinates_mapping: _map})
  }

  componentDidMount() {}
  componentWillUnmount() {}
  render() {
    // console.log("@@@@", this.state.hoveredPoints, this.state.table_to_coordinates_mapping)
    return (
      <div style={{ width: 900, margin: "auto", position: "relative" }}>
        <img  onLoad={this.onImgLoad} src={this.props.imageUrl} class="image" />
        {/* <div style={{ position: "absolute", left: "300px" }}>
          <h>{this.state.imageLoaded ? "True" : "false"}</h>
        </div> */}
        {this.state.imageLoaded ? (
        <div onMouseDown={e => {this.setStateWrapper({is_mousedown:true})}} onMouseUp={e => {this.setStateWrapper({is_mousedown:false})}} class="overlay">
            {
              <React.Fragment>
                {/* {
                  this.testrow(1)
                } */}
                <Grid removeHoveredPoints={this.removeHoveredPoints} rows_columns_data={this.state.rows_columns_data} addToHoveredPoints={this.addToHoveredPoints} imageDimentions={this.state.imageDimentions} is_mousedown={this.state.is_mousedown} />
            </React.Fragment>
            }
            </div>
        ) : null}

        {this.state.imageLoaded ? (
        <div onMouseDown={e => {this.setStateWrapper({is_mousedown:true})}} onMouseUp={e => {this.setStateWrapper({is_mousedown:false})}} class="overlay-new">
            {
              <React.Fragment>
                {/* {
                  this.testrow(2)
                } */}
                <Grid removeHoveredPoints={this.removeHoveredPoints} rows_columns_data={this.state.rows_columns_data} addToHoveredPoints={this.addToHoveredPoints} imageDimentions={this.state.imageDimentions} is_mousedown={this.state.is_mousedown} />
            </React.Fragment>
            }
            </div>
        ) : null}

        {/* {this.state.imageLoaded ? (
          <div
            style={{ position: "absolute", left: "10px", top: -10 }}
            class="overlay-new"
          >
            {this.tablerow()}
          </div>
        ) : null} */}
        <form onSubmit={this.handleSubmit}>
            <label>
              Pick your favorite flavor:
              <select value={this.state.value} onChange={this.handleChange}>
                {/* <option value="lime">Lime</option>
                <option value="coconut">Coconut</option>
                <option value="mango">Mango</option> */}
                {
                  this.state.labels.map((value)=> {
                    return(<option value="mango">{value}</option>)} )
                }
              </select>
            </label>
            <input type="submit" value="Submit" />
        </form>
        <div style={{borderStyle:'solid', borderWidth:1, borderColor:'green', height:6}}>
          <span style={{display:'inline-block', height:2, width:2, backgroundColor:'green', top:10}}></span>
        </div>
      </div>
    );
  }
}
